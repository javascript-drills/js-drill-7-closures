function cacheFunction(cb) {
    const cache = {};

    return function(argumet){
        if(typeof(argumet)!='number'){
            return "Wrong argument passed";
        }
        else{
            if(cache[argumet] != undefined){
                console.log("Argument already present in cache");
                return cache[argumet];
            }
            else{
                return cache[argumet] = cb(argumet);
            }
        }
    }
}
module.exports = cacheFunction;