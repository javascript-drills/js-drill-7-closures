function counterFactory() {
    let counter = 0;
    let obj = {
        incrementOperator : function(){
            if(counter<100){
                return counter = counter+1;
            }
            else{
                return "Cannot go beyond 100";
            }
        },
        decrementOperator : function(){
            if(counter>0){
                return counter = counter-1;
            }
            else{
                return "Cannot go beyond 0";
            }
            
        }
    };
    return obj;
}
module.exports = counterFactory;