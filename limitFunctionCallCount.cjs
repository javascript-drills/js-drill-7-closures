function limitFunctionCallCount(cb, n) {
 
    function callBackNtimes(){ 
            if( n < 1){
                console.log("Invoking limit reached");
                return null;
            }
            else{
                cb(n);
                n--;
            }
    }
    return callBackNtimes;
}
module.exports = limitFunctionCallCount;