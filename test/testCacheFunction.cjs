const cacheFunction = require("../cacheFunction.cjs");

const answer = cacheFunction(callBackFunction);

function callBackFunction(arg){
    console.log(`Argument ${arg} is not present in cache.`);
    return arg*arg;
}

console.log(answer("s"));
console.log(answer(4));
console.log(answer(5));
console.log(answer(6));
console.log(answer(7));
console.log(answer(5));
console.log(answer(4));