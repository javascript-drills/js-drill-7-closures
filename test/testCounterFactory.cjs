const counterFactory = require ("../counterFactory.cjs");

const result = counterFactory();

console.log(result.decrementOperator());
console.log(result.incrementOperator());
console.log(result.incrementOperator());
console.log(result.incrementOperator());
console.log(result.decrementOperator());
console.log(result.incrementOperator());
